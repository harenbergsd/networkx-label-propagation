import copy
import random


def coprarank(G, MAX_ITER, disjoint=False):
    """ My combination of COPRA and LabelRank algorithm. G should be a networkx graph object."""
    
    # Initializations
    nodes = G.nodes()
    labels = nodes[:]                   # each node has a unique label (its own ID)
    P = {v:{v:1} for v in nodes}        # holds the label distributions for each node
    comm_counts = get_comm_counts(P)    # holds number of vertices in each comm
    for v in G.nodes_iter():            # add self loop for every vertex in G 
        G.add_edge(v,v)
    
    max_degree = max([G.degree(v)-1 for v in nodes])  # -1 to not double count self edge
    cutoff_threshold = min(0.1, 1/float(max_degree)-0.000001)
    
   
    for t in range(MAX_ITER):
        Pcopy = copy.deepcopy(P)    # copy label distribution for synchronous updates
        
        for v in nodes:     # node order doesn't matter with synchronous updates
            Pv = {}         # holds the new distribution for v
            
            # Receive label distributions from neighbors
            Nv = G.neighbors(v)
            for neighbor in Nv:
                for label in Pcopy[neighbor].keys():
                    if not label in Pv:
                        Pv[label] = 0.
                    Pv[label] += Pcopy[neighbor][label]
            Pv = {v:Pv[v]/len(Nv) for v in Pv}
            P[v] = Pv   # update distribution for v

            
            lrank_inflate(P[v], 3)  # inflates the distribution
            lrank_cutoff(v, P[v], cutoff_threshold) # removes low probability labels
            
            # Renormalize (because of cutoff)
            valsum = sum(P[v].values())
            for l in P[v].keys():
                P[v][l] = P[v][l]/valsum
            
        cc_new = get_comm_counts(P)     
        if comm_counts == cc_new:   # if no communities have changed sizes exit loop
            break
        else:   # community has changed size
            comm_counts = cc_new 
    
    #print ("Iterations = ", t)
    return comms_from_dist(P, 0, disjoint)



def lrank_inflate(labels, power):
    """ Increases higher probabilities while decreasing lower """
    denom = sum([val**power for val in labels.values()])
    for l in labels.keys():
        labels[l] = labels[l]**power/denom



def lrank_cutoff(v, labels, thresh):
    """ Removes any labels that have probability below threshold """
    for l in labels.keys():
        if labels[l] < thresh:
            del labels[l]



def get_comm_counts(P):
    """ Return counts for each label (community) """
    comm_counts = {}
    for v in P:
        for label in P[v]:
            if not label in comm_counts:
                comm_counts[label] = 0
            comm_counts[label] += 1
    return comm_counts 



def comms_from_dist(P, threshold, disjoint=False):
    """ Return overlapping communities given a distribution of vertices to labels."""
    
    # For disjoint, nodes belong to the label with largest value
    nodes = P.keys()
    if disjoint==True:
        comms = {l:[] for l in nodes}
        for v in nodes:
            label = max(P[v], key=P[v].get)
            comms[label].append(v)
        comms = [comm for comm in comms.values() if len(comm)>0]
        return comms, P
    
    # For overlapping, get communities based on distributions
    # These steps are taken from COPRA with some modifications for cliques
    comms = {}   # list of communities
    subs = {}    # used for subset check
    for v in P:
        vlabels = set([l for l in P[v] if P[v][l] >= threshold])
        for l in vlabels:
            if l in comms:
                comms[l].append(v)
                subs[l] &= vlabels
            else:
                comms[l] = [v]
                subs[l] = copy.deepcopy(vlabels)

    # Determine non-subset communities
    valid_labels = []
    for l in subs:                      # loop over present labels
        subs[l].remove(l)
        if len(subs[l]) == 0:
            valid_labels.append(l)
        else:
            for l2 in subs[l]:
                subs[l2] -= set([l])    # modification in case of cliques
    
    return [comms[l] for l in valid_labels]




def slpa(G, MAX_ITER, r):
    """ SLPA algorithm. G should be a networkx graph object """
    nodes = G.nodes()
    P = {v:{v:1} for v in nodes}    # holds the label distributions for each node
    for t in range(MAX_ITER):
        random.shuffle(nodes)       # asynchronous means the order should be randomized for each iteration 
        for v in nodes:
            label_list = [speaker_random(P[u]) for u in G.neighbors(v)]   # take labels from each of its neighbors
            label = listener_majority(label_list)   # select one of these labels
            if not label in P[v]:
                P[v][label] = 0
            P[v][label] += 1
    
    # Calculate normalized distributions
    for v in nodes:
        P[v] = {l:P[v][l]/float(MAX_ITER+1) for l in P[v]}

    return comms_from_dist(P, r)



def speaker_random(label_dist):
    """ Randomly sends a label based on label distribution """
    bag = []
    for l in label_dist:
        bag += [l]*label_dist[l]
    return random.sample(bag,1)[0]



def listener_majority(labels):
    """ Selects the majority label, random in case of a tie """
    maxval = max(list(labels), key=labels.count)
    randomval = labels.count(random.sample(labels,1)[0])
    if maxval == randomval:
        return randomval
    return maxval
