Community detection via label propagation for [Networkx](https://networkx.github.io/) (a python package for graph analytics).

Here, we will collect various community detection algorithms using *label propagation* methods.

Current algorithms:

* SLPA
* Coprarank: combination of copra and labelrank algorithms.

## USAGE

To use as a python library, import this labelprop package. Example:

``` python
import networkx as nx
import labelprop
G = nx.barbell_graph(5,2)
labelprop.coprarank(G,10)
```